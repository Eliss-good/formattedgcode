import pandas as pd
from transformData import CommandPoint


def formatterCommandData(point: str, command: str, injectionCommand: str | None) -> CommandPoint:

    commandList = [command]
    if injectionCommand:
        commandList = commandList + [injectionCommand, command]

    return {'name': str(point), 'commands': commandList}




def load(pathFile: str):
    df = pd.read_excel(pathFile)
    df = df.where((pd.notnull(df)), None)
    commandData = df.get(['Точка', 'Команда', 'Укол'])

    returnCommandData = list(map(lambda x: formatterCommandData(x.get('Точка'), x.get('Команда'), x.get('Укол')),
                    commandData.to_dict('records')))

    return returnCommandData, df.get('Траектории').dropna().to_list()