from loadExcel import load
from transformData import transformData
from saveGcode import saveGcode

returnCommandData, trajectoriesList = load('./file.xlsx')

allCommandData = []
for oneTrajectories in trajectoriesList:
    allCommandData = allCommandData + transformData(oneTrajectories, returnCommandData)

saveGcode(allCommandData)
