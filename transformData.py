from typing import List, TypedDict


class CommandPoint(TypedDict):
    name: str
    commands: List[str]


def transformData(trajectories: str, commandPoint: List[CommandPoint]):
    trajectories = trajectories.split('-')

    returnCommands = []
    for oneTrajectories in trajectories:
        findCommand = next(onePoint for onePoint in commandPoint if onePoint.get('name') == oneTrajectories)
        if findCommand:
            returnCommands = returnCommands + findCommand.get('commands') or []

    return returnCommands