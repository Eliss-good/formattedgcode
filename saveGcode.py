from typing import List


def saveGcode(allCode: List[str]):
    with open("program.gcode", "w") as file:
        commandToStr = ''
        for oneCode in allCode:
            commandToStr = commandToStr + f"{oneCode}\n"
        strSave= f"%\nG21\n{commandToStr}M02\n%"
        file.write(strSave)